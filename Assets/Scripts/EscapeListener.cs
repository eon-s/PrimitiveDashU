﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class EscapeListener : MonoBehaviour {
	
	public void Update(){
		if (Input.GetKeyDown(KeyCode.Escape)) {
			if (SceneManager.GetActiveScene().name == "MainMenuScene")
				Application.Quit();
			else
				SceneManager.LoadScene("MainMenuScene"); 
		}
	}

}
