﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

/*
 * Carga niveles y ordena el layout de la interfaz de usuario
*/
public class StageLoader : MonoBehaviour {

	public GameObject defaultStage;

	void Start () {
		GameObject stage;
		if (ContentLoader.currentLevel == -1) {
			stage = Instantiate(defaultStage);
			(GameObject.Find("HeroCube").GetComponent("CubeController") as MonoBehaviour).enabled = false;
			(GameObject.Find("HeroCube").GetComponent("CubeControllerEX") as MonoBehaviour).enabled = true;
		} else {
			stage = Instantiate(Resources.Load<GameObject>("Levels/Level"+ContentLoader.currentLevel));
		}
		stage.SetActive(true);
		GameObject.Find("HeroCube").BroadcastMessage("RePosition");
		Text label = GameObject.Find("UILevel").GetComponent<Text>() as Text;
		label.text = "Level " + ContentLoader.currentLevel;

		GameObject.Find("UIButtonBack").transform.localScale = new Vector2(0.3f,0.3f);
		RectTransform rt = GameObject.Find("UIButtonBack").GetComponents<RectTransform>()[0];

		rt.anchorMin = rt.anchorMax = rt.pivot = new Vector2(0.0f, 1.0f);

		GameObject.Find("UIButtonRetry").transform.localScale = new Vector2(0.3f,0.3f);
		rt = GameObject.Find("UIButtonRetry").GetComponents<RectTransform>()[0];
		rt.anchorMin = rt.anchorMax = rt.pivot = new Vector2(1.0f, 1.0f);
		/*
		if (ContentLoader.currentLevel == -1) {
			(GameObject.Find("HeroCube").GetComponent("CubeController") as MonoBehaviour).enabled = false;
		//(GameObject.Find("HeroCube").GetComponent("Cube Controller EX") as MonoBehaviour).enabled = true;
		} else {
			//(GameObject.Find("HeroCube").GetComponent("Cube Controller") as MonoBehaviour).enabled = false;
			(GameObject.Find("HeroCube").GetComponent("CubeControllerEX") as MonoBehaviour).enabled = false;
		}*/
	}

	public void ReStart(){
		GameObject.Find("UICompletion").GetComponent<Text>().enabled = false;

		GameObject.Find("UIButtonBack").transform.localScale = new Vector2(0.3f,0.3f);
		RectTransform rt = GameObject.Find("UIButtonBack").GetComponents<RectTransform>()[0];

		rt.anchorMin = rt.anchorMax = rt.pivot = new Vector2(0.0f, 1.0f);

		GameObject.Find("UIButtonRetry").transform.localScale = new Vector2(0.3f,0.3f);
		rt = GameObject.Find("UIButtonRetry").GetComponents<RectTransform>()[0];
		rt.anchorMin = rt.anchorMax = rt.pivot = new Vector2(1.0f, 1.0f);

		GameObject.Find("HeroCube").BroadcastMessage("Reset");
	}

	void CompletionLayout(){
		GameObject.Find("UICompletion").GetComponent<Text>().enabled = true;

		GameObject.Find("UIButtonBack").transform.localScale = new Vector2(1,1);
		RectTransform rt = GameObject.Find("UIButtonBack").GetComponents<RectTransform>()[0];

		rt.anchorMin = rt.anchorMax = rt.pivot = new Vector2(0.0f, 0.5f);

		GameObject.Find("UIButtonRetry").transform.localScale = new Vector2(1,1);
		rt = GameObject.Find("UIButtonRetry").GetComponents<RectTransform>()[0];
		rt.anchorMin = rt.anchorMax = rt.pivot = new Vector2(1.0f, 0.5f);
	}

}
