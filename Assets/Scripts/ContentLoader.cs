﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
/*
* Carga y organiza el menu principal
*/
public class ContentLoader : MonoBehaviour {

	static public int currentLevel = -1;

	public GameObject levelButton;
	public GameObject levelSelectScroll;

	void Start () {
		//No se debe realizar así porque consume muchos recursos, aparentemente hay que utilizar custom builds
		int totalLevels = Resources.LoadAll("Levels").Length;
		Resources.UnloadUnusedAssets();

		Transform scrollContent = GameObject.Find("Canvas").transform.Find("LevelSelectScroll/Viewport/Content");
		for (int i = 0;i<totalLevels;i++ ){
			CreateButton(scrollContent, i+1);
		}
		levelSelectScroll.SetActive(false);
	}

	public void Load(int level){
		ContentLoader.currentLevel = level;
		SceneManager.LoadScene("GameScene"); 
	}

	/*
	 * Crea botones para incorporar al ScrollView
	*/
	private GameObject CreateButton(Transform parent, int val){
		GameObject go = Instantiate(levelButton) as GameObject;
		go.SetActive(true);
		go.transform.SetParent(parent);
		go.transform.localScale = Vector3.one;

		Button btn = go.GetComponent<Button>();
		btn.GetComponentInChildren<Text>().text = (val).ToString();
		btn.onClick.AddListener(()=>Load(val));

		return go;
	}

}
