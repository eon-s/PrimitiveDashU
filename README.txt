/***
EONS
C�tedra: Manipulaci�n de objetos en 3D
	UNL - FICH - TDPVJ
Fecha: 07-02-2016


Por defecto, en la escena GameScene, carga el nivel asignado a DefaultScene, este nivel debe tener el formato que se puede apreciar en el prefab (Assets/Art/Prefabs/Prototype/)Playground.
Si se quiere editar un nivel se puede agregar a la escena, aplicar cambios y eliminarlo (o desactivarlo).

Los niveles que se cargan en un juego normal son los que figuran en Resources.
El modo de carar los "resources" no es el ideal pero no se si un "custom build" podr�a traer alg�n inconveniente as� que decid� dejarlo de ese modo.

Hay dos scripts para controlar el cubo, uno es normal, el otro experimental (incorpora un elemento de "salto" y un "cambio de gravedad" a la mec�nica), decid� dejarlo en la entrega como muestra de carga din�mica de scripts y uso de los prototipos de nuevos elementos (el juego autom�ticamente habilita uno u otro script seg�n se inicia desde MainMenuScene o GameScene).


Algunas veces, el trigger de colisi�n con un (tag)InstaKill se activa sin raz�n, no encontr� explicaci�n a eso (al principio suced�a en HTML5 y Android pero �ltimamente lo not� en el mismo reproductor del editor), se soluciona al cargar nuevamente el nivel.

El MK Glow System ocasiona un error en shaders del player del editor al cambiar de escena durante la reproducci�n, los juegos compilados funcionan correctamente.

El sonido y m�sica suenan alto en PC pero muy bajo en Android.

***/