﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class SoundPlayer : MonoBehaviour {

	public void PlayDead(){
		GameObject.Find("DeadSound").GetComponent<AudioSource>().Play();
	}

	public void PlayTurn(){
		GameObject.Find("TurnSound").GetComponent<AudioSource>().Play();
	}

	public void PlayComplete(){
		GameObject.Find("CompleteSound").GetComponent<AudioSource>().Play();
	}
}
