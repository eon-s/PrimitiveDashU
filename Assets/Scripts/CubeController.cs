﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/*
Control y manejo de colisiones del cubo.
Movimiento del tipo <stepped>, una unidad en el eje x local, admite touch.
Utiliza Character Controller.
*/

public class CubeController : MonoBehaviour
{

	public float forwardVelocity = 300;
	public float sideVelocity = 300;
	Vector3 startingPosition;
	float moveHorizontal = 0;

	private bool isMoving = false;
	private Vector3 movingDirection;
	private Vector3 startMovingPosition;

	private bool finished = false;

	private bool turn = false;
	private bool dead = false;

	CharacterController cc;
	private float elapsed = 0f;

	void Start ()
	{
		cc = GetComponent<CharacterController> ();
	}

	public void FixedUpdate ()
	{
		if (!finished) {
			moveHorizontal = 0;
			if (turn) {
				transform.position = new Vector3 (Mathf.Round (transform.position.x), transform.position.y, Mathf.RoundToInt (transform.position.z));
				turn = false;
				isMoving = false;
			} else {
				if (cc.isGrounded) {
					Vector3 forwardMove = transform.forward * forwardVelocity;

					Vector3 sideMove = Vector3.zero;

					if (!isMoving) {

						moveHorizontal = (elapsed > 0.5f ? CheckInput () : 0);

						if (!isMoving && moveHorizontal != 0) {
							startMovingPosition = transform.position;
							movingDirection = Vector3.right * System.Math.Sign (moveHorizontal);
							sideMove = transform.TransformDirection (movingDirection * sideVelocity);
							isMoving = true;
							elapsed = 0;
						} 
					} else if (isMoving) {
					
						if (Mathf.Abs (transform.InverseTransformDirection (startMovingPosition).x - transform.InverseTransformDirection (transform.position).x) >= 1f) {
							if (transform.forward == Vector3.forward)
								transform.position = new Vector3 (Mathf.Round (transform.position.x), transform.position.y, transform.position.z);
							else if (transform.forward == Vector3.right)
								transform.position = new Vector3 (transform.position.x, transform.position.y, Mathf.Round (transform.position.z));
							movingDirection = Vector3.zero;
							isMoving = false;
						}
						sideMove = transform.TransformDirection (movingDirection * sideVelocity);
					}

					elapsed += Time.fixedDeltaTime;

					cc.SimpleMove ((forwardMove + sideMove) * Time.fixedDeltaTime);
				} else {				
					cc.Move (Vector3.down * 0.22f);
				}
			}
		} else if (dead) {
			transform.Rotate(Vector3.up*Time.fixedDeltaTime*300);
			transform.localScale-=Vector3.one*Time.fixedDeltaTime;
			if (transform.localScale.x<=0){				
				Reset();
			}
		}
	}

	/*
	 * Los trigger de los items tipo Arrow se revisan con tags, la salida, que es única, por nombre
	 * Todos se revisan contra el segundo collider del HeroCube que se encuentra en otra capa
	 */
	void OnTriggerEnter (Collider col)
	{
		if (col.gameObject.tag == "Arrow") {			
			turn = true;
			transform.rotation = Quaternion.Euler (0, col.transform.rotation.eulerAngles.y + 90, 0);

			isMoving = false;
			moveHorizontal = 0;
			GameObject.Find ("SoundEffects").BroadcastMessage ("PlayTurn");
		} else if (col.gameObject.name == "Goal") {		

			GameObject.Find ("Loader").BroadcastMessage ("CompletionLayout");

			transform.GetComponentInChildren<ParticleSystem> ().transform.localPosition = Vector3.zero;
			var pshape = transform.GetComponentInChildren<ParticleSystem> ().shape;
			pshape.randomDirection = true;
			finished = true;
			GameObject.Find ("SoundEffects").BroadcastMessage ("PlayComplete");
		} 


	}

	/*
	 * Todos los objetos "mortales" que están en el campo de juego llevan el mismo tag
	 */
	void OnControllerColliderHit (ControllerColliderHit hit)
	{
		if (hit.gameObject.tag == "InstaKill") {
			GameObject.Find ("SoundEffects").BroadcastMessage("PlayDead");
			Die();//Reset ();
		}
	}

	/*
	 * CheckInput revisa un <Axis> y, si no hay entrada, revisa si hay inicio de evento Touch
	 */
	private float CheckInput ()
	{
		float rv = 0;
		rv = Input.GetAxis ("Horizontal");

		if ((rv == 0) && (Input.touchCount == 1)) {    
			if (Input.GetTouch (0).phase == TouchPhase.Began) {
					
				rv = (Camera.main.pixelWidth * 0.5 < Input.GetTouch (0).position.x) ? 1 : -1;			

			}

		}    

		return rv;
	}

	/*
	 * startingPosition no se puede modificar en Start ya que el nivel no está cargado, debe modificarse luego de cargar el nivel
	 */
	public void RePosition ()
	{
		startingPosition = GameObject.Find ("PlayerStart").transform.position;	
		transform.position = startingPosition;
	}

	public void Reset ()
	{
		transform.localScale = Vector3.one*0.8f;
		finished = false;
		dead = false;
		transform.position = startingPosition;
		transform.rotation = Quaternion.identity;
		transform.GetComponentInChildren<ParticleSystem> ().transform.localPosition = -Vector3.forward;
		var pshape = transform.GetComponentInChildren<ParticleSystem> ().shape;
		pshape.randomDirection = false;
		moveHorizontal = 0;
		isMoving = false;	
		turn = false;
	}

	public void Die ()
	{
		dead = true;
		finished = true;
	}
}
