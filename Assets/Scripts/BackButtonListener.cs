﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class BackButtonListener : MonoBehaviour {

	public void Back(){
		SceneManager.LoadScene("MainMenuScene"); 
	}

}
