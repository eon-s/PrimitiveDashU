﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class CreditsButtonListener : MonoBehaviour {

	public void LoadCredits(){
		SceneManager.LoadScene("CreditsScene"); 
	}

}
