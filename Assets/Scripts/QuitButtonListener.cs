﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class QuitButtonListener : MonoBehaviour {

	public void Quit(){
		Application.Quit();
	}

}
